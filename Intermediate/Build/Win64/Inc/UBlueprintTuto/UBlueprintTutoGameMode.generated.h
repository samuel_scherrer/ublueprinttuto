// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	C++ class header boilerplate exported from UnrealHeaderTool.
	This is automatically generated by the tools.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectBase.h"

#ifdef UBLUEPRINTTUTO_UBlueprintTutoGameMode_generated_h
#error "UBlueprintTutoGameMode.generated.h already included, missing '#pragma once' in UBlueprintTutoGameMode.h"
#endif
#define UBLUEPRINTTUTO_UBlueprintTutoGameMode_generated_h

#define AUBlueprintTutoGameMode_EVENTPARMS
#define AUBlueprintTutoGameMode_RPC_WRAPPERS
#define AUBlueprintTutoGameMode_RPC_WRAPPERS_NO_PURE_DECLS \
	static inline void StaticChecks_Implementation_Validate() \
	{ \
	}


#define AUBlueprintTutoGameMode_CALLBACK_WRAPPERS
#define AUBlueprintTutoGameMode_INCLASS_NO_PURE_DECLS \
	private: \
	static void StaticRegisterNativesAUBlueprintTutoGameMode(); \
	friend UBLUEPRINTTUTO_API class UClass* Z_Construct_UClass_AUBlueprintTutoGameMode(); \
	public: \
	DECLARE_CLASS(AUBlueprintTutoGameMode, AGameMode, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, UBlueprintTuto, NO_API) \
	DECLARE_SERIALIZER(AUBlueprintTutoGameMode) \
	/** Indicates whether the class is compiled into the engine */    enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	UObject* _getUObject() const { return const_cast<AUBlueprintTutoGameMode*>(this); }


#define AUBlueprintTutoGameMode_INCLASS \
	private: \
	static void StaticRegisterNativesAUBlueprintTutoGameMode(); \
	friend UBLUEPRINTTUTO_API class UClass* Z_Construct_UClass_AUBlueprintTutoGameMode(); \
	public: \
	DECLARE_CLASS(AUBlueprintTutoGameMode, AGameMode, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, UBlueprintTuto, NO_API) \
	DECLARE_SERIALIZER(AUBlueprintTutoGameMode) \
	/** Indicates whether the class is compiled into the engine */    enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	UObject* _getUObject() const { return const_cast<AUBlueprintTutoGameMode*>(this); }


#define AUBlueprintTutoGameMode_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUBlueprintTutoGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUBlueprintTutoGameMode) \
private: \
	/** Private copy-constructor, should never be used */ \
	NO_API AUBlueprintTutoGameMode(const AUBlueprintTutoGameMode& InCopy); \
public:


#define AUBlueprintTutoGameMode_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUBlueprintTutoGameMode(const class FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private copy-constructor, should never be used */ \
	NO_API AUBlueprintTutoGameMode(const AUBlueprintTutoGameMode& InCopy); \
public: \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUBlueprintTutoGameMode)


#undef UCLASS_CURRENT_FILE_NAME
#define UCLASS_CURRENT_FILE_NAME AUBlueprintTutoGameMode


#undef UCLASS
#undef UINTERFACE
#if UE_BUILD_DOCS
#define UCLASS(...)
#else
#define UCLASS(...) \
AUBlueprintTutoGameMode_EVENTPARMS
#endif


#undef GENERATED_UCLASS_BODY
#undef GENERATED_BODY
#undef GENERATED_IINTERFACE_BODY
#define GENERATED_UCLASS_BODY() \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AUBlueprintTutoGameMode_RPC_WRAPPERS \
	AUBlueprintTutoGameMode_CALLBACK_WRAPPERS \
	AUBlueprintTutoGameMode_INCLASS \
	AUBlueprintTutoGameMode_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_POP


#define GENERATED_BODY() \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AUBlueprintTutoGameMode_RPC_WRAPPERS_NO_PURE_DECLS \
	AUBlueprintTutoGameMode_CALLBACK_WRAPPERS \
	AUBlueprintTutoGameMode_INCLASS_NO_PURE_DECLS \
	AUBlueprintTutoGameMode_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_POP


